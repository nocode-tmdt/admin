# build stage
FROM node:14-alpine as build-stage

WORKDIR /app

COPY ./code/package.json ./code/yarn.lock ./
RUN yarn install
COPY ./code/ .
RUN yarn build

# production stage
FROM nginx:stable-alpine as production-stage

EXPOSE 80

COPY default.conf /etc/nginx/conf.d/
COPY --from=build-stage /app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]